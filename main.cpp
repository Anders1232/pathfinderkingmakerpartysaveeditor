#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include "Companions.hpp"

enum class Action
{
	NOT_SPECIFIED,
	TABULATE,
	UN_TABULATE,
	INTERATIVE_EDIT_PARTY
};
//typedef enum ACTIONS Action;

enum class KaessiChoice
{
	NOT_SPECIFIED,
	NOT_CHOSEN,
	KANERAH_D_ELEMENTARIST,
	KALIKKE_KINETICIST,
	NONE,
	BOTH
};

class Instruction
{
	public:
		Instruction(void):action(Action::NOT_SPECIFIED), inputFileName(), outputFileName(){};
		Instruction(Action ac, std::string& inputFile, std::string& outputFile): action(ac), inputFileName(inputFile), outputFileName(outputFile){};
		Action action;
		std::string inputFileName;
		std::string outputFileName;
};

void Identar(FILE* f, int num)
{
	for(int i =0; i < num; i++)
	{
		putc('\t', f);
	}
}

void TabulateFile(FILE *arqEntrada, FILE *arqSaida)
{
	int tabCounter=0;
	char a;
	while (EOF != (a= getc (arqEntrada) ) )
	{
		if('(' == a || '[' == a || '{' == a)
		{
			tabCounter++;
			putc(a, arqSaida);
			putc('\n',arqSaida);
			Identar(arqSaida, tabCounter);
		}
		else if(')' == a || ']' == a || '}' == a)
		{
			tabCounter--;
			putc('\n',arqSaida);
			Identar(arqSaida, tabCounter);
			putc(a, arqSaida);
		}
		else if(','== a)
		{
			putc(a, arqSaida);
			putc('\n',arqSaida);
			Identar(arqSaida, tabCounter);
		}
		else
		{
			putc(a, arqSaida);
		}
	}
}

void UntabulateFile(FILE *arqEntrada, FILE *arqSaida)
{
	char a;
	while (EOF != (a= getc (arqEntrada) ) )
	{
		if('\n' != a  && '\t' != a)
		{
			putc(a, arqSaida);
		}
	}
}

int FindArgument(char const *argument, int argc, char** argv)
{
	for(int i=0; i < argc; i++)
	{
		if(!strcmp(argument, argv[i]))
		{
			return i;
		}
	}
	return -1;
}

int GetcAndPutc (FILE *in, FILE *out)
{
	char aux= getc (in);
	putc(aux, out);
	return aux;
}

int FindPartycharactersEntry(FILE *in)
{
	char aux;
	do{
		aux= getc(in);
		if('\"' == aux )
		{
			if('P' == ( aux=getc(in) ) )		// "PartyCharacters"
			{
				if('a' == ( aux=getc(in) ) )
				{
					if('r' == ( aux=getc(in) ) )
					{
						if('t' == ( aux=getc(in) ) )
						{
							if('y' == ( aux=getc(in) ) )
							{
								if('C' == ( aux=getc(in) ) )// Here we know that we found what we wanted, the entry of curremt party characters
								{
									fscanf(in, "haracters\":[");
									if(ferror(in))
									{
										continue;
									}
									else{
										return 0; //SUCCESS
									}
								}
							}
						}
					}
				}
			}
		}
	}
	while(!feof(in));
	return -1;	// NOT FOUND
}

int FindPartycharactersEntry(FILE *in, FILE *out)
{
/*
 *	*/
	char aux;
	do{
		aux= GetcAndPutc(in, out);
		if('\"' == aux )
		{
			if('P' == ( aux=GetcAndPutc(in, out) ) )		// "PartyCharacters"
			{
				if('a' == ( aux=GetcAndPutc(in, out) ) )
				{
					if('r' == ( aux=GetcAndPutc(in, out) ) )
					{
						if('t' == ( aux=GetcAndPutc(in, out) ) )
						{
							if('y' == ( aux=GetcAndPutc(in, out) ) )
							{
								if('C' == ( aux=GetcAndPutc(in, out) ) )// Here we know that we found what we wanted, the entry of curremt party characters
								{
									fscanf(in, "haracters\":[");
									if(ferror(in))
									{
										continue;
									}
									else{
										fprintf(out, "haracters\":[");
										return 0; //SUCCESS
									}
								}
							}
						}
					}
				}
			}
		}
	}
	while(!feof(in));
	return -1;	// NOT FOUND
}


int FindRemoteCompanionsEntry(FILE *in)//RemoteCompanions
{
	char aux;
	do{
		aux= getc(in);
		if('R' == aux )
		{
			if('e' == ( aux=getc(in) ) )		// "RemoteCompanions"
			{
				if('m' == ( aux=getc(in) ) )
				{
					if('o' == ( aux=getc(in) ) )
					{
						if('t' == ( aux=getc(in) ) )
						{
							if('e' == ( aux=getc(in) ) )
							{
								fscanf(in, "Companions\":[");
								if(ferror(in))
								{
									continue;
								}
								else{
									return 0; //SUCCESS
								}
							}
						}
					}
				}
			}
		}
	}
	while(!feof(in));
	return -1;	// NOT FOUND
}

int FindRemoteCompanionsEntry(FILE *in, FILE *out)//RemoteCompanions
{
	char aux;
	do{
		aux= GetcAndPutc(in, out);
		if('R' == aux )
		{
			if('e' == ( aux=GetcAndPutc(in, out) ) )		// "RemoteCompanions"
			{
				if('m' == ( aux=GetcAndPutc(in, out) ) )
				{
					if('o' == ( aux=GetcAndPutc(in, out) ) )
					{
						if('t' == ( aux=GetcAndPutc(in, out) ) )
						{
							if('e' == ( aux=GetcAndPutc(in, out) ) )
							{
								fscanf(in, "Companions\":[");
								if(ferror(in))
								{
									continue;
								}
								else{
									fprintf(out, "Companions\":[");
									return 0; //SUCCESS
								}
							}
						}
					}
				}
			}
		}
	}
	while(!feof(in));
	return -1;	// NOT FOUND
}

int AdvanteToEndColchetes(FILE *in, FILE *out)
{
	char aux;
	do
	{
		aux = getc(in);
	}
	while (']' != aux );
	putc(']', out);
	return 0;
}


int FillPartyCharactersVector(FILE *in, std::vector<std::string> &partyCharactersCodes)
{
	char s[38];
	memset(s, 0, 38);
	do
	{
		if ( 1 == fscanf(in, "{\"m_UniqueId\":\"%36s\"},", s) )
		{
			std::cout << "Found character: " << s << std::endl;
			partyCharactersCodes.push_back(s);
		}
		else
		{
			std::cout << "Ended finding characters" << std::endl;
			break;
		}
	}
	while(0 == ferror(in));
	return 0;
}

bool ReturnYesOrNoAswer(void)
{
	char aux;
	do
	{
		std::cout<< "Y for yes and N for No" << std::endl;
		aux = getchar();
		if('y' == aux || 'Y' ==  aux)
		{
			return true;
		}
		if('n' == aux || 'N' ==  aux)
		{
			return false;
		}
	}
	while(true);
}

KaessiChoice TheKaessiChoice(KaessiChoice sisterRead)
{
	static KaessiChoice choice= KaessiChoice::NOT_CHOSEN;
	char aux;
	while(KaessiChoice::NOT_CHOSEN == choice)
	{
		std::cout << "About the Kaessi sisters, what you want?"<< std::endl;
		std::cout << "\tA for KanerAh\n";
		std::cout << "\tE for KalikkE\n";
		std::cout << "\tC for the current (" << ( (KaessiChoice::KALIKKE_KINETICIST==sisterRead)?"Kalikke":"Kanerah" ) << ")\n";
		std::cout << "\tN for Neither\n";
		std::cout << "\tB for Both\n";
		std::cout << "\tR for a Random sister" << std::endl;
		aux= getchar();
		if('A' == aux || 'a' == aux)
		{
			choice = KaessiChoice::KANERAH_D_ELEMENTARIST;
		}
		else if('E' == aux || 'e' == aux)
		{
			choice = KaessiChoice::KALIKKE_KINETICIST;
		}
		else if('N' == aux || 'n' == aux)
		{
			choice = KaessiChoice::NONE;
		}
		else if('B' == aux || 'b' == aux)
		{
			choice = KaessiChoice::BOTH;
		}
		else if('C' == aux || 'c' == aux)
		{
			choice = sisterRead;
		}
		else if('R' == aux || 'r' == aux)
		{
//			std::seed_seq seed (clock() );  // seed using question
			std::default_random_engine generator;
			std::bernoulli_distribution distribution(0.5);
			bool result = distribution(generator);
			if(result)
			{
				choice = KaessiChoice::KALIKKE_KINETICIST;
			}
			else
			{
				choice =  KaessiChoice::KANERAH_D_ELEMENTARIST;
			}
		}
	}
	return (choice);
}

int ChangeParty(FILE *in, FILE *out)
{
	if(FindPartycharactersEntry(in))
	{
		std::cout << "Error finding party" << std::endl;
		//error
	}
	std::vector<std::string> partyCharactersCodes;
/*	std::vector<std::string> partyCodes(COMPANIONS::COMPANIONS_SIZE);
	partyCodes[0]= "a040ce3d-d962-4aac-af3b-1e6c0d867787";		Valerie
	partyCodes[1]= "cab3aebe-312b-4bb3-8eab-d6e6bbc816a2";		Player*
	partyCodes[2]= "07d5cd17-33f9-4368-ad1a-950070bebb67";		Amiri
	partyCodes[3]= "a85d03f9-918c-4d84-b6d3-0e266fb70d05";		Linzi
	partyCodes[4]= "df51fcc3-47d0-4329-aac8-39079c2f686b";		Octavia
	partyCodes[5]= "05ab519b-9866-4ba7-866a-e7fef2a1bbeb";		Tristian
	partyCodes[6]= "ad2deca5-d8d0-4f66-b305-cb43d450e2f9";		Regongar
	partyCodes[7]= "d4707979-0272-45b7-ab9f-444a0ab4cb09";		Jaethal
	partyCodes[8]= "ab643633-6b50-47a0-8240-904a4674c6a3";		Harrim
	partyCodes[9]= "bc4d9462-442b-4118-b28a-2a86fde6c05c";		Jubilost
	partyCodes[10]= "564ed562-20af-4fd6-b84b-35af826d877c";		Kalikke
	partyCodes[11]= "9df153d7-1f3e-4b42-965f-1fd26f5a135a";		Kanerah
	partyCodes[12]= "9e02d74d-f70a-419e-b122-9afacf1e53eb";		Ekundayo
*/
	std::cout << "Found party characters " << std::endl;
	FillPartyCharactersVector(in, partyCharactersCodes);
	if(FindRemoteCompanionsEntry(in))
	{
		//error
	}
	std::cout << "Found off-party characters " << std::endl;
	FillPartyCharactersVector(in, partyCharactersCodes);
	std::cout << "Starting to select characters" << std::endl;
	std::vector<std::string> wantedCompanionsCode;
	std::vector<std::string> unwantedCompanionsCode;
	for(unsigned int i=0; i < partyCharactersCodes.size();i++)
	{
		if(0 == partyCharactersCodes[i].compare("a040ce3d-d962-4aac-af3b-1e6c0d867787"))
		{
			std::cout << "You want Valerie in your party? ";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("07d5cd17-33f9-4368-ad1a-950070bebb67"))
		{
			std::cout << "You want Amiri in your party? ";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("a85d03f9-918c-4d84-b6d3-0e266fb70d05"))
		{
			std::cout << "You want Linzi in your party? ";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("df51fcc3-47d0-4329-aac8-39079c2f686b"))
		{
			std::cout << "You want Octavia in your party? ";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("05ab519b-9866-4ba7-866a-e7fef2a1bbeb"))
		{
			std::cout << "You want Tristian in your party? ";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("ad2deca5-d8d0-4f66-b305-cb43d450e2f9"))
		{
			std::cout << "You want Regongar in your party? ";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("d4707979-0272-45b7-ab9f-444a0ab4cb09"))
		{
			std::cout << "You want Jaethal in your party? ";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("ab643633-6b50-47a0-8240-904a4674c6a3"))
		{
			std::cout << "You want Harrim in your party? ";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("bc4d9462-442b-4118-b28a-2a86fde6c05c"))
		{
			std::cout << "You want Jubilost in your party? ";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("564ed562-20af-4fd6-b84b-35af826d877c"))
		{
//			std::cout << "You want Kalikke in your party?";
			if(KaessiChoice::KALIKKE_KINETICIST == TheKaessiChoice(KaessiChoice::KALIKKE_KINETICIST) || KaessiChoice::BOTH == TheKaessiChoice(KaessiChoice::KALIKKE_KINETICIST) )
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("9df153d7-1f3e-4b42-965f-1fd26f5a135a"))
		{
//			std::cout << "You want Kanerah in your party?";
			if(KaessiChoice::KANERAH_D_ELEMENTARIST == TheKaessiChoice(KaessiChoice::KANERAH_D_ELEMENTARIST) || KaessiChoice::BOTH == TheKaessiChoice(KaessiChoice::KANERAH_D_ELEMENTARIST) )
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else if(0 == partyCharactersCodes[i].compare("9e02d74d-f70a-419e-b122-9afacf1e53eb"))
		{
			std::cout << "You want Ekundayo in your party?";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
		}
		else//problably the player character
		{
			std::cout << "You want " << partyCharactersCodes[i] <<" in your party? It is probably your character. Yes is recomended";
			if(ReturnYesOrNoAswer())
			{
				wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
			else
			{
				unwantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
			}
//			wantedCompanionsCode.emplace_back(partyCharactersCodes[i]);
		}
	}
	std::cout<<"Starting to generate output" << std::endl;
	rewind(in);
	if(FindPartycharactersEntry(in, out))
	{
		//error
	}
	for(unsigned int i = 0; i< wantedCompanionsCode.size(); )
	{
		fprintf(out, "{\"m_UniqueId\":\"%s\"}", wantedCompanionsCode[i].c_str());
		i++;
		if(i != wantedCompanionsCode.size())
		{
			fprintf(out, ",");
		}
	}
	//TODO: avançar arquivo de entrada até o fehca colchetes, para voltar a avançar ambos os arquivos
	AdvanteToEndColchetes(in, out);
	FindRemoteCompanionsEntry(in, out);
	for(unsigned int i = 0; i< unwantedCompanionsCode.size(); )
	{
		fprintf(out, "{\"m_UniqueId\":\"%s\"}", unwantedCompanionsCode[i].c_str());
		i++;
		if(i != unwantedCompanionsCode.size())
		{
			fprintf(out, ",");
		}
	}
	AdvanteToEndColchetes(in, out);
	char aux= getc(in);
	do
	{
		putc(aux, out);
		aux= getc(in);
	}
	while(EOF != aux);
	return 0;
}


int main (int argc, char **argv)
{
	int aux;
	Instruction instruction;
	if(-1 != (aux = FindArgument(std::string("-t").c_str(), argc, argv ) ) )
	{
		instruction.action = Action::TABULATE;
		instruction.inputFileName = "player.json";
		instruction.outputFileName = "playerT.json";
	}
	else if(-1 != (aux = FindArgument("-u", argc, argv) ) )
	{
		instruction.action = Action::UN_TABULATE;
		instruction.inputFileName = "playerT.json";
		instruction.outputFileName = "playerM.json";
	}
	else if(-1 != (aux = FindArgument("-c", argc, argv) ) )
	{
		instruction.action = Action::UN_TABULATE;
		instruction.inputFileName = "playerT.json";
		instruction.outputFileName = "playerM.json";
	}
	if(-1 != (aux = FindArgument("-i", argc, argv) ) )
	{
		//TODO: Test if argv[i+1] is valid
		instruction.inputFileName= argv[aux+1];
	}
	if(-1 != (aux = FindArgument("-o", argc, argv) ) )
	{
		//TODO: Test if argv[i+1] is valid
		instruction.outputFileName= argv[aux+1];
	}
	if(-1 != (aux = FindArgument("-p", argc, argv) ) )
	{
		//TODO: Test if argv[i+1] is valid
		instruction.action = Action::INTERATIVE_EDIT_PARTY;
	}

	FILE *f1 = NULL;
	f1 = fopen(instruction.inputFileName.c_str(), "r");
	if(NULL == f1)
	{
		std::cout<< "Failed to open input file " << instruction.inputFileName << std::endl;
		return 0;
	}
	FILE *f2 = NULL;
	f2 = fopen(instruction.outputFileName.c_str(), "wx");
	if(NULL == f2)
	{
		char aux;
		std::cout<< "Failed to open " << instruction.outputFileName << ", maybe already exists. Try to overwrite? (Y/N) ";
		aux = getchar();
		if('Y' == aux || 'S' == aux || 'y' == aux || 's' == aux)
		{
			f2 = fopen(instruction.outputFileName.c_str(), "w");
			if(NULL == f2)
			{
				std::cout<< "Could not open " << instruction.outputFileName << ", exiting." << std::endl;
				return 0;
			}
		}
		else
		{
			std::cout << "Nothing done." << std::endl;
			return 0;
		}
	}
	if(Action::TABULATE == instruction.action)
	{
		TabulateFile(f1, f2);
	}
	else if (Action::UN_TABULATE == instruction.action)
	{
		UntabulateFile(f1, f2);
	}
	else if (Action::INTERATIVE_EDIT_PARTY == instruction.action)
	{
		std::cout << "Interactive editing party" << std::endl;
		ChangeParty(f1, f2);
	}
	fclose(f1);
	fclose(f2);
	return 0;
}
